const gulp = require('gulp'),
      sass = require('gulp-sass'),
      autoprefixer = require('gulp-autoprefixer'),
      browserSync = require('browser-sync').create();


const data = {
  css:  './assets/css',
  scss: './assets/sass/**/*.scss'
};

gulp.task('sass', () => {
  gulp.src(data.scss)
    .pipe(sass({
      includePaths: ['scss'],
      outputStyle: 'expanded'
    }))
    .pipe(gulp.dest(data.css));
});

gulp.task('browser-sync', () => {
  browserSync.init({
    proxy: "http://localhost:4000/"
  });
});

const route = (['./**/*.html', './assets/css/*.css', './assets/js/*.js' ]);

gulp.task('watch', () => {
  gulp.watch([data.scss], ['sass']).on('change', browserSync.reload);
  gulp.watch([route]).on('change', browserSync.reload);
});

gulp.task('default', ['watch', 'sass', 'browser-sync']);

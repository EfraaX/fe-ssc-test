Instructions
==============================

Static files are in the src/dist/ folder
=========================================

[See Demo](https://efraax.github.io/spotify/)


This site has been generated with [Jekyll](https://jekyllrb.com/) and other utilities, including:

* [Jekyll](https://jekyllrb.com/)     : Generator of static sites.
* [Sass](https://sass-lang.com/)      : To generate CSS styles autonomously.
* [Gulp](https://gulpjs.com/)         : Automate the work, compiler SASS among other things.
* [Git](https://git-scm.com/)         : Version control.
* [eFrolic](http://efrolicss.com/)    : CSS Framework based on Flexbox, I use for its great flexibility and its modern components. Also, created by me. Weight less than 50KB.
It is a cool and friendly framework, it offers very impressive designs and it is easy to learn, besides that it is good to try new things. The web is filling with the same.

**If you have knowledge of Jekyll please configure as follows:**

Assuming you have the corresponding facilities of Jekyll:

#### Steps in console:

1. Clone repository.
2. Run NPM.

```
npm install
```
3. Run Jekyll

```
bundle exec jekyll serve --watch
```
4. Run gulp

```
gulp
```

Ready your site should be running effectively.


## If you do not know about Jekyll, please check the "dist" folder and launch "index.html"


The files in this folder are already compiled and are static so you can launch them just by clicking on index.html

If you want to see how this HTML was generated, navigate between the folders "includes", "layouts", etc to know a little more.

[Efrain Peralta](https://efraax.github.io/es/)





Soluciones GBH - Frontend Test
==============================

Hello and thank you for taking the time to take this test.

The goal of this test is to see how you work and what you know. The test is simple, so that you can focus on showing your skills and knowledge

## Instructions

1. Clone this repository
2. Create a new `dev` branch
3. Place your code inside `./src/`
4. Do a pull request from the `dev` branch to the `master` branch.
5. Wait for our team to contact you.

When you finish, please do a pull request to `master` and write about your approach in the description. Our team will take some time to review your code and ask questions which you would need to answer.

Let us know if you run into any issues (report an issue in the git project).


## Requirements

We'd like you to implement a static site, it has 2 pages ("Home" and "Get started"), it also has a side menu. Both sections must be responsive.

In the `./design/` folder you will find the .ai and .png that you will be implementing. Inside the `./design/assets/` folder, you will find icons and backgrounds that you'll need.

**Behaviours**

- Menu
  - Open when the menu icon is cliked
  - When it opens, it pushes content to the left (should have a sliding animation)

- Header
  - Secondary menu (seen in the second slide of the homepage) slides down when you reach this second slide and should be fixed to the top. When scrolling up, if you reach slide 1 it should slide up.

- Content
  - All content should be aligned to the center (vertically and horizontally)
  - Each slide should cover the screen


**Code must be in english**

## Evaluation

Must have:

- Valid HTML
- Valid CSS3
- Valid JS (If any)
- Your answers during review
- UTF8 file encoding
- Clean readable code
- Animations with CSS rather than JS

Nice to have (bonus points):
- An informative, detailed description in the PR
- Bootstrap or other frameworks (Should detail why you are using it)
- Setup script (if necessary)
- Using SASS
- Using BEM Convention
- A git history (even if brief) with clear, concise commit messages.


---

Good luck!
